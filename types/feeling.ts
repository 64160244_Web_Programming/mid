export default interface feeling {
  id: number;
  name: string;
  img: string;
  active: boolean;
}
